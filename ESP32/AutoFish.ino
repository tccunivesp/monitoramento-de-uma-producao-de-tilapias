//Programa: Sistema de Controle e Monitoramento de Tanque de Peixes "AutoFish" V2.0.3
//Autor: Igor Savitsky
//Nesta versão, 
//(i) versão da placa HELTEC WiFi LoRa 32(V2), incluindo endereçamento IP fixo para facilitar reconexões.


//Bibliotecas
#include <Wire.h> //biblioteca comunicação I2C
#include <Adafruit_ADS1015.h> //biblioteca conversor ADC
#include <OneWire.h> //biblioteca onewire (para termometro)
#include <DallasTemperature.h>  //biblioteca termometro
#include <WiFi.h> //Biblioteca WiFi
#include <PubSubClient.h> //Biblioteca PubSubClient para comunicação MQTT
#include <SSD1306.h> //Biblioteca Display OLED SSD1306
#include <FS.h> //Biblioteca FS (sistema de arquivos)
#include <SPIFFS.h> //Biblioteca SPIFFS (sistema de arquivos do ESP)
#include <ThingSpeak.h> //Biblioteca ThingSpeak

//Defines MQTT - TÓPICOS ECLIPSE
#define MQTT_TEMP "m4Cr!_Temp"     //tópico MQTT de temperatura
#define MQTT_PH "m4Cr!_PH"     //tópico MQTT de pH
#define MQTT_ENTRADA "m4Cr!_Entrada"     //tópico MQTT de bomba de entrada
#define MQTT_SAIDA "m4Cr!_Saida"     //tópico MQTT de bomba de saída
#define MQTT_NIVEL "m4Cr!_Nivel"     //tópico MQTT de Nível do tanque
#define ID_MQTT  "m4Cr!_Praca"     //id mqtt (para identificação de sessão)

//Define SPIFFS
#define DATA_PATH "/status_reles.txt" //Arquivo onde serão salvos os status dos reles

////Variáveis Arquivo e Estados Reles
File fileWrite;
File fileRead;
String estadoReles;
String estadoRelesLoad;

// Variáveis e Declaração Display OLED
const int DISPLAY_ADDRESS_PIN = 0x3c; //variáveis do display
const int DISPLAY_SDA_PIN = 4;
const int DISPLAY_SCL_PIN = 15;
const int DISPLAY_RST_PIN = 16;
SSD1306 display(DISPLAY_ADDRESS_PIN, DISPLAY_SDA_PIN, DISPLAY_SCL_PIN); //objeto do display
const int lineHeight = 12; // altura da linha (12 = 5 linhas)

//Declaração ADC e Variaveis sensor pH
Adafruit_ADS1115 ads(0x48); //endereçamento ADC
int16_t sensorPH; //leitura bruta de pH pelo ADC em 16 bits
float voltPH = 0.00; //valor de pH em tensão
const float coefPH = 1.00; //coeficiente de cálculo, cf calibração
const float constPH = 0.00; //constante de cálculo, cf calibração
float pH = 0.00; //valor de pH após calibração
const float pHMAX = 8.00; //valor máximo de pH
int contador = 0; //contador para armazenar quantidade de medidas de pH acima do valor máximo

//Variáveis, instancia e referencia termometro
const int TERMOMETRO = 17; //GPIo para comunicação com termômetro
float sensorTEMP; //Valor de temperatura em Celsius
OneWire oneWire(TERMOMETRO);
DallasTemperature sensors(&oneWire);

//Variáveis WIFI
const char* ssid = "MODEM";
const char* password =  "13805024";
IPAddress local_IP(192, 168, 15, 101);
IPAddress gateway(192, 168, 15, 1);
IPAddress subnet(255, 255, 255, 0);
IPAddress primaryDNS(8, 8, 8, 8);
IPAddress secondaryDNS(8, 8, 4, 4);

//Instanciamento e Variáveis MQTT
WiFiClient espClient1; // Cria o objeto espClient1 (comunicação com Eclipse)
WiFiClient espClient2; //Cria o objeto espClient2 (comunicação com ThingSpeak)
PubSubClient ECLIPSE(espClient1); // Instancia o Cliente ECLIPSE passando o objeto espClient1
const char* ECLIPSE_SERVER = "mqtt.eclipseprojects.io"; //URL do broker Eclipse
int ECLIPSE_PORT = 1883; //Porta do Broker Eclipse
char msgTemp[7]; //Variável de mensagem Temperatura ECLIPSE
char msgPH[5]; //Variável de mensagem pH ECLIPSE
unsigned long THGSPK_CHANNEL = 1229532; //ID do Canal ThingSpeak
const char* WriteAPIKey = "CT958IQEFRXK15U0"; //Chave de Escrita no Canal ThingSpeak


//Variáveis e PINOS de CONTROLE
const int ENTRADA = 12; //GPIO do relê de controle da bomba de entrada
const int SAIDA = 13; //GPIO do relê de controle da bomba de entrada
const int NIVELMIN = 34; //GPI do nível mínimo da bóia do tanque
const int NIVELMAX = 35; //GPI do nível máximo da bóia do tanque
  
//Funções
void initSerial();
void reconectaWiFi();
void initMQTT();
bool displayBegin();
void displayConfig();
void reconectaECLIPSE();
void monitoraConexoes(void);
void monitoraSensores();
void displayOLED();
void publicaMQTT();
void carregaReles();
void escreveReles();
void esgotaTanque();
void encheTanque();
 

//***SETUP (Inicialização)***
void setup() {
  
  //inicia monitor serial
  initSerial();
  
  //Inicia Display OLED
  if(!displayBegin()){
    Serial.println("SSD1306 allocation failed"); //mensagem de falha
    for(;;);
  }
  displayConfig(); //aplica configurações do display

  //Mais funções iniciais
  ads.begin(); //inicia comunicação com ADC
  sensors.begin(); //inicia termometro
  initMQTT(); //inicia MQTT

  //Inicia Pinos de CONTROLE
  pinMode (ENTRADA, OUTPUT); //Pino do relê de controle da bomba de entrada
  pinMode (SAIDA, OUTPUT); //Pino do relê de controle da bomba de entrada
  pinMode (NIVELMAX, INPUT); //Pino do nível máximo da bóia do tanque
  pinMode (NIVELMIN, INPUT); //Pino do nível mínimo da bóia do tanque
    
  //Inicia Display OLED
  if(!displayBegin()){
    Serial.println("SSD1306 allocation failed"); //mensagem de falha
    for(;;);
  }
  displayConfig(); //aplica configurações do display

  

  //Inicia SPIFFS e Carrega estado dos relês
  if(SPIFFS.begin(true))
  {
    carregaReles(); 
  }
  else
  {
    //Se não conseguiu inicializar
    Serial.println("Montagem do SPIFFS falhou!!!"); 
  }

  
  //MULTITAREFA!!!
  xTaskCreatePinnedToCore(loop2, "loop2", 8192, NULL, 1, NULL, 0);//Tarefa "loop2()", prioridade 1, core 0
  
  //Delay para conferir funcionamento do SPIFFS e "lembrança dos reles"
  delay (10000);
} 

//***LOOP 1*** - Cuida do monitoramento e controle do tanque
void loop() {
    //CADEIA DE IFs PARA TOMAR AS AÇÕES CONFORME O ESTADO DOS RELÊS/BOMBAS

    //Estado 0 - Bombas desligadas - Monitoramento de pH e temperatura
    if (estadoReles == "0"){
      monitoraSensores(); //monitora sensores

      //Se medida de pH acima do máximo, incrementa contador 
      if (pH > pHMAX){
        contador++;
      }
      //Se medida sucessiva de pH abaixo do máximo, zera contador
      else{
        contador = 0;
      }

      //imprime estado do contador no monitor serial para debug
      Serial.print("Contador: ");
      Serial.println(contador);

      //Se atingidas 10 medidas sucessivas de pH acima do máximo, atuação sobre as bombas
      if (contador >= 10){
        
        //Se tanque no nível máximo, liga bomba de saída e altera estado para 2
        if (digitalRead(NIVELMAX) == HIGH){
          digitalWrite (SAIDA, HIGH);
          digitalWrite (ENTRADA, LOW);
        }
        //Se tanque abaixo do nível máximo, liga bomba de entrada para completar o tanque
        else {
          digitalWrite (SAIDA, LOW);
          digitalWrite (ENTRADA, HIGH);
        }
        contador = 0;
      }

      escreveReles(); //escreve estado atual dos relês para continuação do loop e recuperação de estados
    }

    //Estado 1 - Bomba de entrada ligada
    else if (estadoReles == "1"){
      
      encheTanque(); //chama função de enchimento do tanque
            
    }
    //Estado 2 - Bomba de saída ligada
    else if (estadoReles == "2"){
      
      esgotaTanque(); //chama função de esvaziamento do tanque
      encheTanque(); //chama função de enchimento do tanque
            
    }

    //Leitura de outros estados ou erro de recuperação do arquivo
    else {
      
      estadoReles = "0"; //volta estado dos relês para 0
      monitoraSensores(); //monitora sensores
      escreveReles(); //escreve estado atual dos relês para continuação do loop     
    }     
    
   
    delay(60000); //pausa de 1 minuto entre os ciclos

}

//***LOOP 2*** - Cuida das conexões WiFi e MQTT
void loop2(void*z) {

  //Laço infinito
  while(1){
  monitoraConexoes(); //Chama Função de monitoramento de conexões
  delay(20000); //pausa de 20s entre verificações
  }
  
}

  //###FUNÇÕES DE INÍCIO DO SISTEMA###
  //inicia monitor serial
  void initSerial(){ 
    Serial.begin(9600);
  }

    // Função que inicializa o display
    bool displayBegin(){
      // Reiniciamos o display
      pinMode(DISPLAY_RST_PIN, OUTPUT);
      digitalWrite(DISPLAY_RST_PIN, LOW);
      delay(1);
      digitalWrite(DISPLAY_RST_PIN, HIGH);
      delay(1);

      return display.init(); 
    }

    // Função que faz algumas configuções no display
    void displayConfig(){
      display.flipScreenVertically(); // Invertemos o display verticalmente
      display.setFont(ArialMT_Plain_10); // Setamos a fonte
      display.setTextAlignment(TEXT_ALIGN_LEFT); // Alinhamos a fonte à esquerda
      }


  //Inicia Comunicação MQTT
  void initMQTT(){
    ECLIPSE.setServer(ECLIPSE_SERVER, ECLIPSE_PORT);   //informa qual broker e porta deve ser conectado a Eclipse
    ThingSpeak.begin(espClient2);     //Inicia Thingspeak
  }

  //Carrega estado dos reles do arquivo e aplica setup nos relês
  void carregaReles(){
    
    //Abre arquivo contendo estado dos reles e transporta conteúdo do arquivo para variável
        fileRead = SPIFFS.open(DATA_PATH, FILE_READ);
    if(!fileRead){
      //Erro na abertura do arquivo
      Serial.println("Abertura do arquivo falhou!!!");
      return;
    } 
    estadoRelesLoad = fileRead.readString(); 
    fileRead.close();

    //Abre arquivo contendo estado dos reles e imprime conteúdo do arquivo
    fileRead = SPIFFS.open(DATA_PATH, FILE_READ);
    if(!fileRead){
      //Erro na abertura do arquivo
      Serial.println("Abertura do arquivo falhou!!!");
      return;
    }
    while (fileRead.available()) {
    Serial.print("Conteudo do arquivo: ");
    Serial.print(fileRead.readString()); //
    }  
    fileRead.close();
    
    //imprime valor da variável para comparação
    Serial.println(" ");
    Serial.print("Valor da variável: ");
    Serial.println(estadoRelesLoad);
    Serial.println("Estado dos Relês lido com sucesso.");
        
    //IFs para escrever nos pinos os estados gravados
    if (estadoRelesLoad == "1"){
      digitalWrite (ENTRADA, HIGH);
      digitalWrite (SAIDA, LOW);
      Serial.println("Estado Relês Lido = 1 (Entrada ON, Saida OFF)");
    }

    else if (estadoRelesLoad == "2"){
      digitalWrite (ENTRADA, LOW);
      digitalWrite (SAIDA, HIGH);
      Serial.println("Estado Relês Lido = 2 (Entrada OFF, Saida ON)");
    }

    else if (estadoRelesLoad == "0"){
      digitalWrite (ENTRADA, LOW);
      digitalWrite (SAIDA, LOW);
      Serial.println("Estado Relês Lido = 0 (Entrada OFF, Saida OFF)");
    }

    else {
      estadoRelesLoad = "0";
      digitalWrite (ENTRADA, LOW);
      digitalWrite (SAIDA, LOW);
      Serial.println("Estado Relês Lido = 0 (Entrada OFF, Saida OFF)");
    }

    estadoReles = estadoRelesLoad; //atualiza variável para rodar o loop principal

    //confere atualização de variável para debug
    Serial.print("Transportou e acertou?: ");
    Serial.println(estadoReles);
    
  }
  
  //###LOOP1 - FUNÇÕES DE MONITORAMENTO E CONTROLE DE PH E TEMPERATURA###
  
  //Escreve estado dos reles no arquivo
  void escreveReles(){
    
    //IFs para ler estado dos pinos e definir variável
    if (digitalRead(ENTRADA) == HIGH){
      
      if (digitalRead(SAIDA) == LOW){
      estadoReles = "1";
      Serial.println("Estado Relês Atual = 1 (Entrada ON, Saida OFF)");
      }
      else {
      estadoReles = "3";
      Serial.println("Estado Relês Atual = 3 (Entrada ON, Saida ON)");
      }
    }
    else {
      
      if (digitalRead(SAIDA) == LOW){
      estadoReles = "0";
      Serial.println("Estado Relês Atual = 0 (Entrada OFF, Saida OFF)");
      }
      else {
      estadoReles = "2";
      Serial.println("Estado Relês Atual = 2 (Entrada OFF, Saida ON)");
      }
    }

    //Escreve valor da variável no arquivo
    fileWrite = SPIFFS.open(DATA_PATH, FILE_WRITE);
    if(!fileWrite){
      // Erro na escrita do arquivo
      Serial.println("Abertura do arquivo falhou!!!");
      return;
    } 
    else {
      fileWrite.seek(0);
      fileWrite.print(estadoReles);
      fileWrite.close();
      Serial.println("Estado dos Relês gravado com sucesso.");
    }
  }
  
  //Monitoramento e publicação de informações dos sensores
  void monitoraSensores(){
    
    //requisita dados do termômetro
    sensors.requestTemperatures(); 
    sensorTEMP = sensors.getTempCByIndex(0);

    //requisita dados do sensor de pH
    sensorPH = ads.readADC_SingleEnded(0);  
    voltPH = (sensorPH * 0.1875)/1000;
    pH = (voltPH*coefPH) + constPH;

    //publica dados no display OLED e no broker MQTT
    displayOLED();
    publicaMQTT();
  }
 
  //Display OLED e Monitor Serial
  void displayOLED(){
    int line;
    display.clear(); //limpa display
    
    //imprime versão do Sistema
    line = 0;
    display.drawString(0, line, "Auto Fish 2.0.3  ");
    display.drawString(97, line, String(voltPH));
    line++;

    //imprime status do wifi
    if (WiFi.status() != WL_CONNECTED){
      display.drawString(0, line * lineHeight, "WiFi OFF");
    }
    else {
      display.drawString(0, line * lineHeight, "WiFi ON ");
    }

    //imprime pH
    display.drawString(49, line * lineHeight, " | ");
    display.drawString(67, line * lineHeight, "pH: ");
    display.drawString(91, line * lineHeight, String(pH));
    Serial.print("pH: ");
    Serial.println(pH);
    line++;
        
    //imprime temperatura
    display.drawString(0, line * lineHeight, "Temp: ");
    display.drawString(37, line * lineHeight, String(sensorTEMP));
    display.drawString(79, line * lineHeight, "ºC");
    Serial.print("Termometro: ");
    Serial.print(sensorTEMP);
    Serial.println(" ºC");
    line++;

    //imprime status das bombas
    if (digitalRead(ENTRADA) == HIGH){
      display.drawString(0, line * lineHeight, "Enche ON");
      Serial.println("Entrada ON");
    }
    else {
      display.drawString(0, line * lineHeight, "Enche OFF");
      Serial.println("Entrada OFF");
    }

    display.drawString(52, line * lineHeight, " | ");
    
    if (digitalRead(SAIDA) == HIGH){
      display.drawString(64, line * lineHeight, "Saida ON");
      Serial.println("Saida ON");
    }
    else {
      display.drawString(64, line * lineHeight, "Saida OFF");
      Serial.println("Saida OFF");
    }
    line++;

    //imprime status da boia
    if (digitalRead(NIVELMAX) == HIGH){
      display.drawString(0, line * lineHeight, "Nível MAX");
      Serial.println("Tanque no nível máximo");
    }
    else if (digitalRead(NIVELMIN) == HIGH){
      display.drawString(0, line * lineHeight, "Nível MIN");
      Serial.println("Tanque no nível mínimo");
    }
    else {
      display.drawString(0, line * lineHeight, "Nível MED");
      Serial.println("Tanque em nível médio");
    }
        
    //escreve dados no display
    display.display();
  }

  //Publicações MQTT
  void publicaMQTT(){

    //publica Eclipse temperatura
    dtostrf (sensorTEMP, 7, 2, msgTemp);
    ECLIPSE.publish(MQTT_TEMP, msgTemp);
    Serial.print("ECLIPSE Temperatura: ");
    Serial.println(msgTemp);    
    
    //publica Eclipse pH
    dtostrf (pH, 5, 2, msgPH);
    ECLIPSE.publish(MQTT_PH, msgPH);
    Serial.print("ECLIPSE pH: ");
    Serial.println(msgPH);

    //publica Eclipse status Entrada
    if (digitalRead(ENTRADA) == HIGH){
      ECLIPSE.publish(MQTT_ENTRADA, "ON");
      Serial.println("ECLIPSE Entrada: ON");
    }
    else {
      ECLIPSE.publish(MQTT_ENTRADA, "OFF");
      Serial.println("ECLIPSE Entrada: OFF");
    }

    //publica Eclipse status Saída
    if (digitalRead(SAIDA) == HIGH){
      ECLIPSE.publish(MQTT_SAIDA, "ON");
      Serial.println("ECLIPSE Saida: ON");
    }
    else {
      ECLIPSE.publish(MQTT_SAIDA, "OFF");
      Serial.println("ECLIPSE Saida: OFF");
    }

    //publica Eclipse status da boia
    if (digitalRead(NIVELMAX) == HIGH){
      ECLIPSE.publish(MQTT_NIVEL, "MAX");
      Serial.println("ECLIPSE Nivel: MAX");
    }
    else if (digitalRead(NIVELMIN) == HIGH){
      ECLIPSE.publish(MQTT_NIVEL, "MIN");
      Serial.println("ECLIPSE Nivel: MIN");
    }
    else {
      ECLIPSE.publish(MQTT_NIVEL, "MED");
      Serial.println("ECLIPSE Nivel: MED");
    }

    //PUBLICAÇÕES THINGSPEAK
    ThingSpeak.setField(1, pH); //campo do pH
    ThingSpeak.setField(2, sensorTEMP); //campo da Temperatura

    //campo da Bomba de Entrada
    if (digitalRead(ENTRADA) == HIGH){
       ThingSpeak.setField(3, 1);}
    else {
       ThingSpeak.setField(3, 0);}

    //campo da Bomba de Saída
    if (digitalRead(SAIDA) == HIGH){
       ThingSpeak.setField(4, 1);}
    else {
       ThingSpeak.setField(4, 0);}

    //publica Eclipse status da boia
    if (digitalRead(NIVELMAX) == HIGH){
      ThingSpeak.setField(5, 2);}
    else if (digitalRead(NIVELMIN) == HIGH){
      ThingSpeak.setField(5, 0);}
    else {
      ThingSpeak.setField(5, 1);}

    //manda dados dos campos para o ThingSpeak
    int x = ThingSpeak.writeFields(THGSPK_CHANNEL, WriteAPIKey);
        if(x == 200){
          Serial.println("THGSPK OK!");
        }
        else{
          Serial.println("THGSPK ERRO! CODE: " + String(x));
        }
  }

  //Função de Enchimento do Tanque
  void encheTanque(){

    //Laço - enquanto nível do tanque abaixo do máximo, mantém bomba de entrada ligada e continua sensoriamento    
    while (digitalRead(NIVELMAX) != HIGH){
      
      Serial.println("Tanque Enchendo...");
      
      if (digitalRead(ENTRADA) != HIGH){
        digitalWrite (ENTRADA, HIGH);
      }
      
      if (digitalRead(SAIDA) != LOW){
        digitalWrite (SAIDA, LOW);
      }

      monitoraSensores(); //monitora sensores
      escreveReles(); //escreve estado atual dos relês para continuação do loop e recuperação de estados
      delay(60000); //pausa de 1 minuto entre os ciclos
      
    }
    //Após bóia sinalizar tanque cheio, desliga bomba de entrada
    Serial.println("Tanque Cheio, desligando bomba de entrada...");
    digitalWrite (SAIDA, LOW);
    digitalWrite (ENTRADA, LOW);
    
    monitoraSensores(); //monitora sensores
    escreveReles(); //escreve estado atual dos relês para continuação do loop e recuperação de estados
    delay(60000); //pausa de 1 minuto para voltar ao loop principal  
  }

  // Função de esvaziamento do tanque
  void esgotaTanque(){

    //Laço - enquanto nível do tanque acima do mínimo, mantém bomba de saída ligada e continua sensoriamento        
    while (digitalRead(NIVELMIN) != HIGH){

      Serial.println("Tanque Esvaziando...");
      
      if (digitalRead(ENTRADA) != LOW){
        digitalWrite (ENTRADA, LOW);
      }
      
      if (digitalRead(SAIDA) != HIGH){
        digitalWrite (SAIDA, HIGH);
      }
      
      monitoraSensores(); //monitora sensores
      escreveReles(); //escreve estado atual dos relês para continuação do loop e recuperação de estados
      delay(60000); //pausa de 1 minuto entre os ciclos
    }

    //Após bóia sinalizar tanque em nível mínimo, desliga bomba de saida e liga bomba de entrada
    Serial.println("Tanque no nivel minimo, desligando bomba de saida e ligando bomba de entrada...");
    digitalWrite (SAIDA, LOW);
    digitalWrite (ENTRADA, HIGH);
    monitoraSensores(); //monitora sensores
    escreveReles(); //escreve estado atual dos relês para continuação do loop e recuperação de estados
    delay(60000); //pausa de 1 minuto para voltar ao loop principal
  }
  
  
  //LOOP2 - FUNÇÕES DE MONITORAMENTO DE WIFI E MQTT
  //Função de monitoramento das conexões
  void monitoraConexoes(void){
    if (!ECLIPSE.connected()){ 
        reconectaECLIPSE();} //Chama Função de reconexão de MQTT Eclipse
    else{ 
     reconectaWiFi();} //Chama Função de reconexão de WIFI
     delay(15000);
     }
      
  //Função de reconexão de WIFI
  void reconectaWiFi() {
    delay(10);

    if (!WiFi.config(local_IP, gateway, subnet, primaryDNS, secondaryDNS)) {
      Serial.println("Falha na configuração do IP Fixo");
      }
        
    if (WiFi.status() != WL_CONNECTED){
    
        Serial.println("------Conexao WI-FI------");
        Serial.print("Conectando à rede: ");
        Serial.println(ssid);
        Serial.print("Aguarde");
       
        WiFi.begin(ssid, password); // Conecta na rede WI-FI
     
        while (WiFi.status() != WL_CONNECTED) 
        {
          delay(1000);
          Serial.print(".");
                    
        }
   
        Serial.println();
        Serial.print("Conectado com sucesso na rede ");
        Serial.println(ssid);
        Serial.print("IP obtido: ");
        Serial.println(WiFi.localIP());
        
        }

    else{
      reconectaECLIPSE();
    }
  }

  //Função de reconexão do MQTT
  void reconectaECLIPSE(){ 
    while (!ECLIPSE.connected()){
    
        Serial.println("Conectando ao broker ECLIPSE...");
        
        if (ECLIPSE.connect(ID_MQTT)){
          Serial.println("Conectado ao broker ECLIPSE!");
        }
        else{
          Serial.print("Falha na conexao ao broker ECLIPSE - Estado: ");
          Serial.println(ECLIPSE.state());
          Serial.println("Havera nova tentativa de conexao em 10s");
          delay(10000);
          reconectaWiFi();
        }
        delay(15000);
    }
  }

  
